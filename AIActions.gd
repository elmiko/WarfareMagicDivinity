extends HBoxContainer

signal action_selected(action)

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var _actions = [$Divinity, $Magic, $Warfare]
var _rejected_count = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func enable_all():
	for a in _actions:
		a.enable()


func start_selection_process():
	_rejected_count = 0
	$Timer.start()

func _on_Timer_timeout():
	if _rejected_count == 2:
		for a in _actions:
			if a.enabled:
				$Timer.stop()
				emit_signal("action_selected", a)
	else:
		var a = _random_action()
		while not a.enabled:
			a = _random_action()
		a.enabled = false
		_rejected_count += 1


func _random_action():
	return _actions[randi() % _actions.size()]
