extends Node2D

const version = "1.0.0"
const DIVINITY = "Divinity"
const MAGIC = "Magic"
const WARFARE = "Warfare"

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var _game_over = true
var _player_action: Action = null


# Called when the node enters the scene tree for the first time.
func _ready():
	$Version.text = version
	randomize()
	$MainMenu.show()


func _on_Action_clicked(action):
	if action.highlighted:
		action.stop_progress_timer()
		action.highlighted = false
		action.reset_progress()
		_enable_all_player_actions()
		_player_action = null
	else:
		$PlayerActions/Warfare.disable()
		$PlayerActions/Magic.disable()
		$PlayerActions/Divinity.disable()
		action.enable()
		action.highlighted = true
		action.start_progress_timer()
		_player_action = action


func _on_Action_progress_max(action: Action):
	_player_action = action
	$AIActions.start_selection_process()
	


# This is the main game logic, called after the AI has selected its action
func _on_AIActions_action_selected(action):
	var message = "This message does not exist"
	match _player_action.name:
		DIVINITY:
			match action.name:
				MAGIC: # Victory
					var h = _player_action.level * 2
					$PlayerHealth.add(h)
					message = "Divinity Victory! Gain " + str(h) + " health"
					_player_action.level_up()
				DIVINITY: # Draw
					$PlayerHealth.add(_player_action.level)
					$AIHealth.add(action.level)
					message = "Draw. Gain " + str(_player_action.level) + " health, opponent gains " + str(action.level) + " health"
				WARFARE: # Loss
					var d = action.level - $PlayerArmor.level
					d = 0 if d < 0 else d
					$PlayerHealth.add(-d)
					$AIArmor.add(1)
					message = "Loss to Warfare. Take " + str(d) + " damage, opponent gains 1 armor"
					action.level_up()
				_:
					message = "Error! unknown action name " + action.name
		MAGIC:
			match action.name:
				WARFARE: # Victory
					var d = _player_action.level * 2 - $AIArmor.level
					d = 0 if d < 0 else d
					$AIHealth.add(-d)
					message = "Magic Victory! Deal " + str(d) + " damage"
					_player_action.level_up()
				MAGIC: # Draw
					var d = _player_action.level - $AIArmor.level
					d = 0 if d < 0 else d
					$AIHealth.add(-d)
					var rd = action.level - $PlayerArmor.level
					rd = 0 if rd < 0 else rd
					$PlayerHealth.add(-rd)
					message = "Draw. Deal " + str(d) + " damage, take " + str(rd) + " damage"
				DIVINITY: # Loss
					var h = action.level * 2
					$AIHealth.add(h)
					message = "Loss to Divinity. Opponent gains " + str(h) + " health"
					action.level_up()
		WARFARE:
			match action.name:
				DIVINITY: # Victory
					var d = _player_action.level - $AIArmor.level
					d = 0 if d < 0 else d
					$AIHealth.add(-d)
					$PlayerArmor.add(1)
					message = "Warfare Victory! Deal " + str(d) + " damage and gain 1 armor"
					_player_action.level_up()
				WARFARE: # Draw
					var d = 1 if 1 - $AIArmor.level > 0 else 0
					var rd = 1 if 1 - $PlayerArmor.level > 0 else 0
					$AIHealth.add(-d)
					$PlayerHealth.add(-rd)
					message = "Draw. Deal " + str(d) + " damage and take " + str(rd) + " damage"
				MAGIC: # Loss
					var d = action.level * 2 - $PlayerArmor.level
					d = 0 if d < 0 else d
					$PlayerHealth.add(-d)
					message = "Loss to Magic. Take " + str(d) + " damage"
					action.level_up()

	if $AIHealth.value <= 0 or $PlayerHealth.value <= 0:
		_game_over = true
		message += "\nGame Over, "
		if $AIHealth.value <= 0 and $PlayerHealth.value <= 0:
			message += "Draw"
		elif $AIHealth.value <= 0:
			message += "Victory"
		else:
			message += "Defeat"
	$GameMessage.show_post_action_process_message(message)


func _enable_all_player_actions():
	$PlayerActions/Warfare.enable()
	$PlayerActions/Magic.enable()
	$PlayerActions/Divinity.enable()

func _reset_all_action_levels():
	$PlayerActions/Warfare.reset_level()
	$PlayerActions/Magic.reset_level()
	$PlayerActions/Divinity.reset_level()
	$AIActions/Warfare.reset_level()
	$AIActions/Magic.reset_level()
	$AIActions/Divinity.reset_level()


func _reset_players():
	$AIHealth.set_health(3)
	$AIArmor.set_armor(0)
	$PlayerHealth.set_health(3)
	$PlayerArmor.set_armor(0)
	_enable_all_player_actions()
	$AIActions.enable_all()
	if _player_action != null:
		_player_action.reset_progress()
		_player_action = null
	_reset_all_action_levels()
	


func _on_NewGameButton_pressed():
	_game_over = false
	_reset_players()
	$MainMenu.hide()


func _on_Menu_player_click():
	if _game_over == true:
		$MainMenu.show()
	else:
		_enable_all_player_actions()
		$AIActions.enable_all()
		if _player_action != null:
			_player_action.reset_progress()
	$GameMessage.hide()


func _on_InstructionsButton_pressed():
	$Instructions.show()


func _on_Instructions_gui_input(event):
	if event is InputEventMouseButton:
		if event.pressed:
			$Instructions.hide()
