extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var message setget set_message, get_message
var _message = ""

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func get_message():
	return _message


func set_message(value: String):
	_message = value
	if value == "":
		$Message.hide()
		$MessageBackground.hide()
	else:
		$Message.text = value
		$MessageBackground.show()
		$Message.show()
