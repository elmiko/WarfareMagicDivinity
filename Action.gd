class_name Action
extends Control

const HIGHLIGHT = Color("#ffffffff")
const NORMAL = Color("64ffffff")
const PROGRESS_TIMER_SECS = 1.5

signal action_clicked(action)
signal progress_max(action)

export var action_icon = "res://icon.png"
export var action_name = "Name"
export var enable_highlight = true

var enabled = true setget enabled_set, enabled_get
var highlighted = true setget highlight_set, highlight_get
var level = 1

var _highlight = false
var _action_enabled = true


func _ready():
	$Name.text = action_name
	$Level.text = str(level)
	$Icon.texture = load(action_icon)
	$Timer.wait_time = PROGRESS_TIMER_SECS / float($TextureProgress.max_value)


func disable():
	_action_enabled = false
	$DisabledRect.show()
	$DisabledRect.mouse_filter = MOUSE_FILTER_IGNORE


func enable():
	_action_enabled = true
	$DisabledRect.hide()
	$DisabledRect.mouse_filter = MOUSE_FILTER_STOP


func enabled_get():
	return _action_enabled


func enabled_set(value: bool):
	if value:
		enable()
	else:
		disable()


func highlight_get():
	return _highlight


func highlight_set(value: bool):
	_highlight = value
	if _highlight:
		$Outline.show()
	else:
		$Outline.hide()


func level_up():
	level += 1
	$Level.text = str(level)


func reset_level():
	level = 1
	$Level.text = str(level)


func reset_progress():
	$TextureProgress.value = 0
	highlight_set(false)
	


func start_progress_timer():
	$Timer.start()


func stop_progress_timer():
	$Timer.stop()


func _on_Action_gui_input(event):
	if not _action_enabled:
		return
	if event is InputEventMouseButton:
		if event.pressed:
			emit_signal("action_clicked", self)


func _on_Action_mouse_entered():
	if not _action_enabled:
		return
	if enable_highlight:
		$Outline.show()


func _on_Action_mouse_exited():
	if not _action_enabled:
		return
	if enable_highlight and not _highlight:
		$Outline.hide()


func _on_Timer_timeout():
	if $TextureProgress.value < $TextureProgress.max_value:
		$TextureProgress.value += 1
	else:
		_action_enabled = false
		stop_progress_timer()
		emit_signal("progress_max", self)
