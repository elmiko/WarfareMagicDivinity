VERSION = 1.0.0
FILENAME = WarfareMagicDivinity-$(VERSION)
LINUX_FILENAME = $(FILENAME)-linux.x86_64


clean:
	rm -rf build


release-linux:
	mkdir -p build/linux
	godot --no-window --export 'Linux/X11' --path `pwd` build/linux/$(LINUX_FILENAME)
	sha512sum build/linux/$(LINUX_FILENAME) > build/linux/$(LINUX_FILENAME).sha512
	bzip2 build/linux/$(LINUX_FILENAME)
