extends Control

var level = 0 setget set_armor, get_armor
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var _armor = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
# refresh the health display
func add(value: int):
	_armor += value
	refresh()


func refresh():
	$Label.text = str(_armor)

func get_armor():
	return _armor


# set the health number
func set_armor(value: int = 0):
	_armor = value
	refresh()
