# Warfare, Magic, Divinity

A simple game inspired by Rock, Paper, Scissors.

[Try it out here](https://wmd.opbstudios.com)

This project build using the [Godot Engine](https://www.godotengine.org), to
run it either download a binary release from the
[releases page](https://gitlab.com/elmiko/WarfareMagicDivinity/-/releases), or run
the project using the appropriate
[Godot binary for your platform](https://godotengine.org/download).

![demo video](https://opbstudios.com/warfaremagicdivinity/wmd-demo-video1.gif)
