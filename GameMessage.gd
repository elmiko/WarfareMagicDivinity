extends Control

signal player_click
# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func show_post_action_process_message(message: String):
	$Message.text = message
	self.show()


func _on_Menu_gui_input(event):
	if event is InputEventMouseButton:
		if event.pressed:
			emit_signal("player_click")
