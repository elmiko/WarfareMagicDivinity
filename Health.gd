extends Control

var value setget set_health, get_health
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var _health = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func add(value: int):
	_health += value
	refresh()


# refresh the health display
func refresh():
	$Label.text = str(_health)

func get_health():
	return _health


# set the health number
func set_health(value: int = 0):
	_health = value
	refresh()
